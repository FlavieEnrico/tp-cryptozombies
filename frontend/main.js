import "./style.css";

// 1. Import modules.
import { createPublicClient, http, getContract, createWalletClient, custom, parseEther } from "viem";
import { goerli } from "viem/chains";
import { CryptoZombies } from "./abi/CryptoZombies";

// 2. Set up your client with desired chain & transport.
const [account] = await window.ethereum.request({ method: "eth_requestAccounts" });
const walletClient = createWalletClient({
  account,
  chain: goerli,
  transport: custom(window.ethereum),
});

const publicClient = createPublicClient({
  chain: goerli,
  transport: http(),
});

const zombieContract = getContract({
  address: "0x39c17646679a1f72c3af670a6efec7b4f0aa91a0",
  abi: CryptoZombies, 
  publicClient,
  walletClient,
});

// 3. Consume an action!
const ownerAddress = await zombieContract.read.owner();
const blockNumber = await publicClient.getBlockNumber();
const balanceOfOwner = await zombieContract.read.balanceOf([account]);

async function zombieExists(zombieId) {
  try {
    await zombieContract.read.zombies([zombieId]);
    return true;
  } catch (error) {
    return false;
  }
}

const zombieIds = [];
let currentZombieId = 0;

while (true) {
  const exists = await zombieExists(currentZombieId);
  if (exists) {
    zombieIds.push(currentZombieId);
    currentZombieId++;
  } else {
    break; 
  }
}

const zombieInfos = [];

for (const zombieId of zombieIds) {
  const info = await zombieContract.read.zombies([zombieId]);
  zombieInfos.push(info);
}

const myZombies = await zombieContract.read.getZombiesByOwner([account]);

const ownerZombies = [];
for (const zombieId of zombieIds) { 
  if (zombieId == myZombies) {
  const info = await zombieContract.read.zombies([zombieId]);
  ownerZombies.push(info);
  }
}

document.querySelector("#app").innerHTML = `
  <div>
    <h1>CryptoZombies Party!</h1>
    <p>Current block number is ${blockNumber}</p>
    <p>Owner address is ${account}</p>
    <p>You are the owner of ${balanceOfOwner} zombie(s).</p>
    <p>Your zombie(s) id(s) is ${myZombies}.</p>
    <h2>My Zombies:</h2>
    <div id="zombieGallery">
    <span id="noZombiesYet"></span>
    </div>
    <h2>Zombie Gallery:</h2>
    <div id="zombieGallery">
    <span id="partyEmpty"></span>
    </div>
    <h2>Get your own new zombie today!</h2>
    <div>
      <form>
        <label for="newZName">My new zombie's name: </label>
        <input type="text" name="newZName" id="newZName"/>
        <button id="createNewZ">Create a new zombie</button>
      </form>
      <span id="newZombieSpan"></span>
    </div>
  </div>
`;

if (myZombies.length == 0) {
  document.querySelector("#noZombiesYet").innerHTML=`No Zombies yet. Get your own today!`;
} else {
  document.querySelector("#noZombiesYet").innerHTML= `${ownerZombies.map((info, index) => 
    `<ul><h3>Zombie ${index + 1}: </h3>
    <li>Name: ${info[0]}</li>
    <li>DNA: ${info[1]}</li>
    <li>Level: ${info[2]}</li>
    <li>Ready Time: ${info[3]}</li>
    <li>Wins: ${info[4]}</li>
    <li>Losses: ${info[5]}</li>
    <br/>
    <button id="levelMyZ">Level Up!</button>
    <span id="levelUpSpan"></span>
    </ul>
    <span id="zombieView"></span>
    `).join('')}`;

    document.querySelector("#zombieView").innerHTML= `
    <div id="displayZombie">
    <img src="zombieparts/left-thigh-1@2x.png" id="left-thigh"></img>
    <img src="zombieparts/left-leg-1@2x.png" id="left-leg"></img>
    <img src="zombieparts/left-feet-1@2x.png" id="left-feet"></img>
    <img src="zombieparts/left-upper-arm-1@2x.png" id="left-upper-arm"></img>
    <img src="zombieparts/left-forearm-1@2x.png" id="left-forearm"></img>
    <img src="zombieparts/hand1-1@2x.png" id="left-hand"></img>
    <img src="zombieparts/right-thigh-1@2x.png" id="right-thigh"></img>
    <img src="zombieparts/right-leg-1@2x.png" id="right-leg"></img>
    <img src="zombieparts/right-feet-1@2x.png" id="right-feet"></img>
    <img src="zombieparts/right-upper-arm-1@2x.png" id="right-upper-arm"></img>
    <img src="zombieparts/right-forearm-1@2x.png" id="right-forearm"></img>
    <img src="zombieparts/hand-2-1@2x.png" id="right-hand"></img>
    <img id="shirt"></img>
    <img id="head"></img>
    <img id="eyes"></img>
    <img src="zombieparts/mouth-1@2x.png" id="mouth"></img>
    <img src="zombieparts/torso-1@2x.png" id="torso"></img>
    </div>
    `;

    document.getElementById("shirt").src=shirtSrc();
    document.getElementById("head").src=headSrc();
    document.getElementById("eyes").src=eyesSrc();

    let LEVEL_UP_COST_AS_STRING = "0.001";

    document.querySelector("#levelMyZ").addEventListener("click", async (event) => {
      event.preventDefault();
      const hash = await zombieContract.write.levelUp([myZombies], {value: parseEther(LEVEL_UP_COST_AS_STRING)});
      document.querySelector("#levelUpSpan").innerHTML=`Waiting for payment...`;
      const transaction = await publicClient.waitForTransactionReceipt({hash: `${hash}`});
      if (transaction.status=="success") {
        document.querySelector("#levelUpSpan").innerHTML=`Your zombie has leveled up!`;
      } else {
        document.querySelector("#levelUpSpan").innerHTML=`Cannot process level up!`;
      };
    });
}

if (zombieInfos.length == 0) {
  document.querySelector("#partyEmpty").innerHTML=`Create a zombie to get the party started!`
} else {
  document.querySelector("#partyEmpty").innerHTML= `${zombieInfos.map((info, index) => 
    `<ul><h3>Zombie ${index + 1}: </h3>
    <li>Name: ${info[0]}</li>
    <li>DNA: ${info[1]}</li>
    <li>Level: ${info[2]}</li>
    <li>Ready Time: ${info[3]}</li>
    <li>Wins: ${info[4]}</li>
    <li>Losses: ${info[5]}</li>
    </ul>`).join('')}`;
}

document.querySelector("#createNewZ").addEventListener("click", async (event) => {
  event.preventDefault();
  const hash = await zombieContract.write.createRandomZombie([document.querySelector("#newZName").value]);
  document.querySelector("#newZombieSpan").innerHTML=`Waiting for new zombie`;
  const transaction = await publicClient.waitForTransactionReceipt({hash: `${hash}`});
  if (transaction.status=="success") {
    document.querySelector("#newZombieSpan").innerHTML=`Your new zombie has arrived!`;
  } else {
    document.querySelector("#newZombieSpan").innerHTML=`No new zombie for you!`;
  };
});

function shirtSrc() {
  let currentDna = String(ownerZombies[0][1]);
  let i = parseInt(currentDna.substr(4, 6)) % 6 + 1;
  console.log("zombieparts/shirt-" + i + "@2x.png");
  return "zombieparts/shirt-" + i + "@2x.png";
};

function headSrc() {
  let currentDna = String(ownerZombies[0][1]);
  let i = parseInt(currentDna.substr(0, 2)) % 7 + 1;
  console.log("zombieparts/head-" + i + "@2x.png");
  return "zombieparts/head-" + i + "@2x.png";
};

function eyesSrc() { 
  let currentDna = String(ownerZombies[0][1]);
  let i = parseInt(currentDna.substr(2, 4)) % 11 + 1;
  console.log("zombieparts/eyes-" + i + "@2x.png");
  return "zombieparts/eyes-" + i + "@2x.png";
};