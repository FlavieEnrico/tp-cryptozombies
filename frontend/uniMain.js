import "./style.css";

// 1. Import modules.
import { createPublicClient, formatUnits, getContract, http, createWalletClient, custom, parseUnits, parseAbiItem, getAddress } from "viem";
import { goerli } from "viem/chains";
import { UNI } from "./abi/UNI";

// 2. Set up your client with desired chain & transport.

const [account] = await window.ethereum.request({ method: "eth_requestAccounts" });
const walletClient = createWalletClient({
  account,
  chain: goerli,
  transport: custom(window.ethereum),
});

const publicClient = createPublicClient({
  chain: goerli,
  transport: http(),
});

const uniContract = getContract({
  address: "0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984",
  abi: UNI, 
  publicClient,
  walletClient,
});

/*await uniContract.write.transfer(["0x2958c3bA0b70C3565702DbB7ebC85ab6F25f5aFa", 1n]);*/

// 3. Consume an action!
const symbol = await uniContract.read.symbol();
const blockNumber = await publicClient.getBlockNumber();
const contractName = await uniContract.read.name();
const decimals = await uniContract.read.decimals();
const totalSupply = formatUnits(await uniContract.read.totalSupply(), decimals);
const myAddress = "0x2958c3bA0b70C3565702DbB7ebC85ab6F25f5aFa";
let myBalance = formatUnits(await uniContract.read.balanceOf([myAddress]), decimals);

document.querySelector("#app").innerHTML = `
  <div>
    <p>Current block is <span id="blockNumber">${blockNumber}</span></p>
    <h1>Token ${symbol}</h1>
    <p>Name: ${contractName}</p>
    <p>Address: <a href="https://goerli.etherscan.io/token/0x1f9840a85d5af5bf1d1762f925bdaddc4201f984">${uniContract.address}</a></p>
    <p>Total Supply: ${totalSupply}</p>
    <p>Balance of ${myAddress}: <span id="balance">${myBalance}</span></p>
    <hr/>
    <form>
      <label for="amount">Amount: </label>
      <input type="text" name="amount" id="amount"/>
      <button id="max">Max</button>
      <br/>
      <label for="recipient">Recipient: </label>
      <input type="text" name="recipient" id="recipient"/>
      <button id="send">Send</button>
    </form>
    <span id="transactionSpan"></span>
  </div>
`;

document.querySelector("#max").addEventListener("click", (event) => {
  event.preventDefault();
  document.querySelector("#amount").value = myBalance;
});

document.querySelector("#send").addEventListener("click", async (event) => {
  event.preventDefault();
  const amount = parseUnits(document.querySelector("#amount").value, decimals);
  const recipient = document.querySelector("#recipient").value;
  const hash = await uniContract.write.transfer([recipient, amount]);
  document.querySelector("#transactionSpan").innerHTML=`Waiting for tx <a href="https://goerli.etherscan.io/tx/${hash}">${hash}</a>`;
  const transaction = await publicClient.waitForTransactionReceipt({hash: `${hash}`});
  if (transaction.status=="success") {
    document.querySelector("#transactionSpan").innerHTML=`Transaction <a href="https://goerli.etherscan.io/tx/${hash}">${hash}</a> confirmed!`;
  } else {
    document.querySelector("#transactionSpan").innerHTML=`Transaction <a href="https://goerli.etherscan.io/tx/${hash}">${hash}</a> failed!`;
  };
});

console.log(goerli);

const unwatch = publicClient.watchBlockNumber( 
  { onBlockNumber: blockNumber => {document.querySelector("#blockNumber").innerHTML = `${blockNumber}`}});


publicClient.watchBlockNumber({
  onBlockNumber: async () => {
  const logs = await publicClient.getLogs({
  address: uniContract.address,
  event: parseAbiItem("event Transfer(address indexed from, address indexed to, uint256 value)"),
    });
    const myTransfers = logs.filter((log) => log.args.from == getAddress(account) || log.args.to == getAddress(account));
    if (myTransfers.length > 0) {
      myBalance = await uniContract.read.balanceOf([account]);
      console.log(`New balance ${myBalance}`);
      document.querySelector("#balance").innerHTML = formatUnits(myBalance, decimals);
    }
  }
})
